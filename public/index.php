<?php

# define constatnts for all the necessary directories
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('VIEW',  ROOT . 'app' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR);
define('MODEL',  ROOT . 'app' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR);
define('DATA',  ROOT . 'app' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR);
define('CORE',  ROOT . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR);
define('CONTROLLER',  ROOT . 'app' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR);

# create array of all the defined constants
$modules = [ROOT, APP, CORE, CONTROLLER, DATA, VIEW, CONTROLLER];

/*
create autoloader for all the defined directories
will look something like this
Array
(
    [0] => C:\xampp\php\PEAR
    [1] => C:\xampp\htdocs\strovaversion1\
    [2] => C:\xampp\htdocs\strovaversion1\app\
    [3] => C:\xampp\htdocs\strovaversion1\app\core\
    [4] => C:\xampp\htdocs\strovaversion1\app\controller\
    [5] => C:\xampp\htdocs\strovaversion1\app\data\
    [6] => C:\xampp\htdocs\strovaversion1\app\view\
    [7] => C:\xampp\htdocs\strovaversion1\app\controller\
)
*/
set_include_path(get_include_path() . PATH_SEPARATOR . implode(PATH_SEPARATOR, $modules));
spl_autoload_register('spl_autoload', false);

# .env loading
require 'C:\xampp\htdocs\strovaversion1\vendor\autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/..');
$dotenv->load();

# go to the main operation
new Application;

?>