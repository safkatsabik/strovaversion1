<!DOCTYPE html>
<html lang="en">
<head>
    <title>Strova</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<style>
    .sysfooter{
        height: 300px;
    }

    .errorHeader{
        height: 75px;
        background-color: #f57167;
        color: white;
        text-align: center;
        font-size: 20px;
    }

    .mainBody{
        text-align: center;
        padding-top: 100px;
    }
</style>
<body style="margin: 0px !important">
    <div class="col-md-12 errorHeader">
        <span>Something went wrong with your code!</span>
    </div>
</body>
</html>
