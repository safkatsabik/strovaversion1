<?php

/*
| This controller is reserved for system error
| reporting to the programmer. This is associated 
| with organized view files so that the programmer 
| doesnt get bored. If you alter this class or delete 
| it, things won't work. Please dont touch this.
| 
| From author: Sabik Safkat-4/11/2020

*/
class systemController extends Controller
{
    public function __construct()
    {

    }


    /*
    | When something is missing, like a controller,
    | method, parameter, view file or any other class, 
    | this is where the exception handling take place. 
    | 
    | From author: Sabik Safkat-4/11/2020
    */
    public function notFoundErrors($errName = '')
    {
        $this->view('systems' . DIRECTORY_SEPARATOR . 'notfound', [
            'name' => $errName
        ]);
        $this->view->render();
    }
}

?>