<?php

class DB
{
    protected $db_conn;
    protected $dbhost;
    protected $dbname;
    protected $dbuser;
    protected $dbpass; 
    public function __construct()
    {
        $dbhost = $_SERVER['DATABASE_HOST'];
        $dbname = $_SERVER['DATABASE_NAME'];
        $dbuser = $_SERVER['DATABASE_USER'];
        $dbpass = $_SERVER['DATABASE_PASSWORD'];
    }

    public function connect()
    {
        try{
            $this->db_conn = new PDO(
                "mysql:host = $this->dbhost; dbname=$this->dbname", 
                $this->dbuser, 
                $this->dbpass
            );
            return $this->db_conn;
        } catch (PDOException $e){
            return $e->gerMessage();
        }
    }
}

?>