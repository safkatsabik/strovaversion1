<?php


class Application
{
    protected $controller = '';
    protected $action = '';
    protected $params = [];
    
    public function __construct()
    {
        $routes = $this->getRoutes();
        $this->parseURI($routes);
        if (file_exists(CONTROLLER . $this->controller.'.php')){
            $this->controller = new $this->controller;
            if (method_exists($this->controller, $this->action)){
                call_user_func_array([$this->controller, $this->action], $this->params);
            } else {
                $this->controller->index();    
            }
        } else {
            $this->controller = new systemController;
            $error = 'doesnt exist in app/controller/' . $this->controller . '.php doesnt exist';
            $this->controller->notFoundErrors($error);
        }
    }

    protected function parseURI($routes)
    {
        /* getting the complete url after domain or root  */
        $request = trim($_SERVER['REQUEST_URI'], '/');

        /* checking if request url is empty. if not, the first string after spliting 
        with '/' will be used as route name. If request uri is empty, then it is 
        assumed that '/' is the route name, wich will practically mean user is browsing
        the home page. If no such route called '/' is defined, error will be thrown. */
        if (!empty($request)){
            $url = explode('/', $request);
        } else {
            $url = ['/'];
        }
        
        $route_to_match = $url[0];
        if (array_key_exists($route_to_match, $routes)){
            $directives = $routes[$route_to_match];
            $breakdowns = explode('/', $directives);
            if (count($breakdowns) != 2){
                echo "invalid param count";
            } else {
                $this->controller = $breakdowns[0].'Controller';        
                $this->action = $breakdowns[1];
                for ($i = 0; $i <= 1; $i++){
                    unset($breakdowns[$i]);
                }  
                $this->params = !empty($url) ? array_values($url) : [];     
            }
        } else {
            $this->controller = 'systemController';
            $this->action = 'notFoundErrors';
            $this->params = ['Route is not defined!'];
        }
    }


    /*
    |   The getRoutes method creates an instance of the Routers class.
    |   Then it requires the routes.php file where all the routes have
    |   been declared. Then by calling Routers class's define method, 
    |   an array of all the routes in routes.php file are loaded in an
    |   array and returned to the constructor.
    */
    protected function getRoutes()
    {
        $routes = new Routers();
        require('routes.php');
        return $routes->routes;
    }
}


?>