<?php

class Routers
{
    public $routes = [];

    /*
    Here we are building a collection of internal url's that contains the path, 
    the controller and the method name. The array will be build on runtime and 
    each request uri will be checked wheather it exists in the array. If yes, proper 
    service will be provided. If not, default controller and method for error display 
    will be served.  
    */

    public function define($uri)
    {
        $this->routes = $uri;
    }
}